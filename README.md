# cms

> A Vue.js project

## Build Setup



``` bash

download repo

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

# Features Added:
    1)Login with Rest
    2)Routes for Landing, Dashboard, Post
    3)UI for Dashboard and Landing
    3)Rest for Post
    4)Quill Editor

# Features Pending:
    1)Google Analytics
    2)Social Media Scheduling
    3)RSS Feed
    4)Salting

# Working Endpoints
    1)Auth: '/api/auth/:email'
    2)Post: '/api/post'

Last Updated: 08/12/2018 04:26 pm

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
