import express from 'express';
import auth from './models/postModel';
const bucketRouter = express.Router();
import Mongo from 'mongodb';

var MongoClient = Mongo.MongoClient;

var uri = "mongodb+srv://superUser:cartel_5@carteldemo-t6wq6.mongodb.net/cms?retryWrites=true";

bucketRouter
// to get featured and hero bucket
  .get('/:page/2', (req, res) => {
      var page = req.params.page;
      var type = req.params.type;
      var resultArr = [] ;
      MongoClient.connect(uri, function(err, client) {
        if (err) res.send('error');
        const db = client.db("cms");
        db.collection(page).find({$or:[{type:'featured'},{type:'hero'}]}, (err, result) => {
          if (err) res.send('error');
          else{
            result.forEach((ele)=>{
              if(ele.type=='hero')
                resultArr.push(ele);
              else{
                ele.content.forEach((elem)=>{
                  db.collection('articles').find({id:elem},(req,re)=>{
                    if(err) res.send(err);
                    else{
                      re.content.forEach((el)=>{
                        if(el.id==elem)
                          resultArr.push(el);
                      })
                    }
                  })
                })
              }
            })
            res.send(resultArr);
          } 
        })
        client.close();
      });
  })
  //to get rest of buckets
  .get('/:page/4', (req, res) => {
    var page = req.params.page;
    var type = req.params.type;
    var resultArr = [] ;
    MongoClient.connect(uri, function(err, client) {
      if (err) res.send('error:'+err);
      const db = client.db("cms");
      db.collection(page).find({$or:[{type:'carousel'},{type:'video'},{type:'tweets'},{type:'freelist'}]}, (err, result) => {
        if (err) res.send('error');
          else{
            result.forEach((ele)=>{
              if(ele.type=='video'||ele.type=='tweets')
                resultArr.push(ele);
              else{
                ele.content.forEach((elem)=>{
                  db.collection('articles').find({id:elem},(req,re)=>{
                    if(err) res.send(err);
                    else{
                      re.content.forEach((el)=>{
                        if(el.id==elem)
                        {
                          el.type = ele.type;
                          resultArr.push(el);
                        }
                      })
                    }
                  })
                })
              }
            })
            res.send(resultArr);
          } 
      })
      client.close();
    });
})
  // to add a new bucket
  .post('/:page/',(req,res) => {
    var page = req.params.page;
    var title = req.body.title;
    var type = req.body.type;
    var limit = req.body.limit;
    MongoClient.connect(uri, function(err, client) {
      if(err) res.send('error:'+err);
      const db = client.db("cms");
      db.collection(page).insertOne({title:title,type:type,content:[],limit:limit,size:0},(err,result)=>{
        if(err) res.send('error');
        else{
          res.send('succcessfull');
        } 
      })
      client.close();
    })
  })
  //to add a an article to bucket
  .post('/add_to_bucket',(req,res)=>{
    var pages = req.body.pages;
    var id = req.body.id;
    pages.forEach((el)=>{
      var page = el.name;
      var newBucket = el.newBucket;
      MongoClient.connect(uri, function(err, client) {
        if(err) res.send('error:'+err);
        const db = client.db("cms");
        db.collection(page).updateOne({type:newBucket},{$push:{content:id}},(err,result)=>{
          if (err) res.send('error');
          else
            res.send('successful');
        })
        client.close();
      })
    })
  })
  // to update a bucket with an article
  .post('/update_bucket',(req,res)=>{
    var pages = req.body.pages;
    var id = req.body.id; 
    pages.forEach((el)=>{
      var page = el.name;
      var prevBucket = el.prevBucket;
      var newBucket = el.newBucket;
      var content = [];
      MongoClient.connect(uri, function(err, client) {
        if(err) res.send('error:'+err);
        const db = client.db("cms");
        db.collection(page).findOne({type:newBucket},(err,result)=>{
          if(err) throw res.send('error');
          else if(result.size == result.limit){
            res.send('Bucket Full')
          }
          else{
            db.collection(page).findOne({type:prevBucket}, (err,result)=>{
              if(err) res.send('error');
              else{
                content = result.content;
                content.forEach((el,index)=> {
                  if(el == id)
                    content.splice(index,1);
                });
              }
            })
            db.collection(page).updateOne({type:prevBucket},{$set:{content:content}},(err,result)=>{
              if(err) res.send('error');
              db.collection(page).updateOne({type:newBucket},{$push:{content:id},$inc:{size:1}},(err,result)=>{
                if(err) res.send('error');
                else{
                  res.send('successfull')
                } 
              })
            })
          }
        })
        client.close();
      })
    })
  })
  // to change the name of bucket
  .put('/:page',(req,res)=>{
    var page = req.params.page;
    var title = req.body.title;
    var type = req.body.type;
    MongoClient.connect(uri, function(err, client) {
      if(err) res.send('error:'+err);
      const db = client.db("cms");
      db.collection(page).updateOne({type:type},{$set:{title:title}},(err,result)=>{
        if(err) throw err;
        else{
          res.send('succcessfull');
        } 
      })
      client.close();
    })
  })
export default postRouter;
