import express from 'express';
const app = express();
import bodyParser from 'body-parser';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


const port = process.env.PORT || 8000;
// routes go here
app.listen(port, () => {
    console.log(`http://localhost:${port}`)
})


import authRouter from './auth/authRouter';
import postRouter from './blogPost/postRouter';
import bucketRouter from './blogPost/bucketRouter';
app.use('/api/auth', authRouter);
app.use('/api/post', postRouter);
app.use('/api/buckets', bucketRouter);



// import Mongo from 'mongodb';

// var MongoClient = Mongo.MongoClient;

// // var uri = "mongodb+srv://superUser:cartel_5@carteldemo-t6wq6.mongodb.net/cms?retryWrites=true";
// var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true"

// MongoClient.connect(uri, function(err, client) {
//     if (err) console.log(err);
//     const db = client.db("cms");
//     db.collection('articles').insertMany([
//         {type:'formula',content:[]},
//         {type:'cricket',content:[]},
//         {type:'tennis',content:[]},
//         {type:'football',content:[]},
//         {type:'basketball',content:[]},
//         ],function(err, result) {
//       if (err) throw err;
//         // res.setHeader('Content-Type', 'application/json');
//         // res.send(result);
//         console.log('success');
//     })
//     client.close();
//   });

