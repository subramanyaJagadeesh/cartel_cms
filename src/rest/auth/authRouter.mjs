import express from 'express';
import auth from './models/authModel';
const authRouter = express.Router();
import Mongo from 'mongodb';

var MongoClient = Mongo.MongoClient;

// var uri = "mongodb+srv://superUser:cartel_5@carteldemo-t6wq6.mongodb.net/cms?retryWrites=true";
var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true"

authRouter
  .post('/:email', (req, res) => {
    var emailRef = req.body.email;
    var pass = req.body.password;
      MongoClient.connect(uri, function(err, client) {
        if (err) console.log(err);
        const db = client.db("cms");
        db.collection('auth').findOne({$and:[{'email' : emailRef},{'password':pass}] }, function(err, result) {
          if (err) throw err;
          else{
            res.setHeader('Content-Type', 'application/json');
            res.send(result);
          } 
        })
        client.close();
      });
  })
export default authRouter;
