import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const authModel = new Schema({
  email: { type: String },
  password: { type: String },
  userType: { type: String }
})
export default mongoose.model('auth', authModel)
