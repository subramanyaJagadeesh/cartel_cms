import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const postModel = new Schema({
  fName: { type: String },
  lName: { type: String },
})
export default mongoose.model('post', postModel)
