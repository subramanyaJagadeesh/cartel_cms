import express from 'express';
import auth from './models/postModel';
const postRouter = express.Router();
import Mongo from 'mongodb';

var MongoClient = Mongo.MongoClient;

var uri = "mongodb+srv://superUser:cartel_5@carteldemo-t6wq6.mongodb.net/cms?retryWrites=true";

postRouter
  //to post a new article
  .post('/articles/:sport', (req, res) => {
      var postObj = req.body;
      var sport = req.params.sport;
      MongoClient.connect(uri, function(err, client) {
        if (err) res.send('error:'+err);
        const db = client.db("cms");
        db.collection('articles').updateOne({type:sport},{ $push: { content: postObj } }, (err, result) => {
          if (err) throw err;
          else
            res.send('www.postgame.in/'+sport+'/'+postObj.name);
        })
        client.close();
      });
  })
  //to get all the articles in archive
  .get('/articles',(req,res) => {
    MongoClient.connect(uri, function(err, client) {
      if(err) res.send('error:'+err);
      const db = client.db("cms");
      db.collection('articles').find((err,result)=>{
        if(err) throw err;
        else{
          res.setHeader('Content-Type', 'application/json');
          res.send(result);
        } 
      })
      client.close();
    })
  })
  //to get a specific article
  .get('/articles/:sport/:id',(req,res)=>{
    var sport = req.params.sport;
    var id = req.params.id;
    MongoClient.connect(uri, function(err, client) {
      if(err) res.send('error:'+err);
      const db = client.db("cms");
      db.collection('articles').find({type:sport},(err,result)=>{
        if(err) throw err;
        else{
          result.content.forEach(element => {
            if (element.id == id){
              res.setHeader('Content-Type', 'application/json');
              res.send(element);
            }
          });
        } 
      })
      client.close();
    })
  })
export default postRouter;
