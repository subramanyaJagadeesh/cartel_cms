const jsonConfig = {
  'template': 1,
  'blog': {
    'class': 'blog',
    'introduction': {
      'class': 'intro',
      'firstBlock': {
        'class': 'intro-first',
        'content': 'intro-first-wrapper',
      },
      'SecondBlock': 'intro-second',
    },
    'description': {
      'class': 'description',
      'firstBlock': 'description-first',
      'SecondBlock': {
        'class': 'description-second',
        'content': 'description-second-wrapper',
      }
    }
  }   
} 
export default jsonConfig;
