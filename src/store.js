import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    email: '',
    userType: '',
  },
  getters: {

  },
  plugins: [createPersistedState()],
  mutations: {
    SET_DETAILS: (state, req) => {
      state.email = req.email,
      state.userType = req.userType
    }
  },
  actions: {

  }
})
