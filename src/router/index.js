import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/views/Landing'
import Dashboard from '@/views/Dashboard'
import Sport from '@/views/Sport'
import Post from '@/views/Post'
import pageBuckets from '@/views/pageBuckets.vue'
import Schedule from '@/views/Schedule.vue'
import Articles from '@/views/Articles.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing,
      meta: {
        show:false
      }
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: {
        show: true
      }
    },
    {
      path: '/schedule',
      name: 'Schedule',
      component: Schedule,
      meta: {
        show: true
      }
    },
    {
      path: '/buckets',
      name: 'pageBuckets',
      component: pageBuckets,
      meta: {
        show: true
      }
    },
    {
      path: '/articles',
      name: 'Articles',
      component: Articles,
      meta: {
        show: true
      }
    },
    {
      path: '/sport',
      name: 'Sport',
      component: Sport,
      meta: {
        show: true
      }
    },
    {
      path: '/post/:id',
      name: 'Post',
      component: Post,
      meta: {
        show: true
      }
    }
  ]
})
